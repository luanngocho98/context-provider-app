import React, { useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { ClearItemOnLocalStorage, KEY } from '../../helper';
import './index.css'

export default function Header({ isAuthentication, setIsAuthentication }) {
  const location = useLocation();
  const [actions, setActions] = useState([]);

  useEffect(() => {
    const { pathname } = location;
    const copyActions = isAuthentication ? [
      {
        id: 1,
        title: 'Home B',
        path: '/',
        isActive: true,
      },
      {
        id: 2,
        title: 'About 1',
        path: '/about',
        isActive: false,
      },
      {
        id: 3,
        title: 'Avatar',
        path: '/profile',
        isActive: false,
        type: 'avatar',
      },
      {
        id: 4,
        title: 'Logout',
        path: '/login',
        isActive: false,
        type: 'logout',
      },
    ] : [
      {
        id: 1,
        title: 'Home B',
        path: '/',
        isActive: true,
      },
      {
        id: 2,
        title: 'About 1',
        path: '/about',
        isActive: false,
      },
      {
        id: 3,
        title: 'Login',
        path: '/login',
        isActive: false,
      },
      {
        id: 4,
        title: 'Register',
        path: '/register',
        isActive: false,
      }
    ];
    for(let i = 0; i < copyActions.length; i++) {
      if(copyActions[i].path === pathname) {
        copyActions[i].isActive = true
      } else {
        copyActions[i].isActive = false
      }
    }
    setActions(copyActions);
  }, [isAuthentication, location]);

  const handleLogout = () => {
    setIsAuthentication(false);
    ClearItemOnLocalStorage(KEY.USER);
  };

  return (
    <div className='header'>
      <div className='logo'>
        <Link to="/">
          <img
            alt="logo"
            src="https://cdn.worldvectorlogo.com/logos/redux.svg"
            style={{
              height: 40,
              width: 40
            }}
          />
        </Link>
      </div>
      {
        actions?.length && (
          <div className='actions'>
            {
              actions.map(action => {
                if(action?.type === 'avatar') {
                  return(
                    <img
                      key={action.id}
                      alt='Fail avatar'
                      src='https://cdn-www.vinid.net/613774c6-lieu-nhac-cua-son-tung-m-tp-con-gay-suc-hut-voi-khan-gia.jpg'
                      className='avatar'
                    />
                  )
                } else if(action.type === 'logout') {
                  return(
                    <Link
                      key={action.id}
                      className={action.isActive && 'active'}
                      onClick={handleLogout}
                      to={action.path}
                    >
                      {action.title}
                    </Link>
                  )
                } else {
                  return(
                    <Link
                      to={action.path}
                      key={action.id}
                      className={action.isActive && 'active'}
                    >
                      {action.title}
                    </Link>
                  )
                }
              })
            }
          </div>
        )
      }
    </div>
  )
}
