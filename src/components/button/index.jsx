import React from 'react'
import { Button } from 'reactstrap'

export default function LoadingButton(props) {
  const { value, onClick, className, color } = props;
  
  return (
    <>
      <Button
        className={className}
        color={color}
        onClick={onClick}
      >
        {value}
      </Button>
    </>
  )
}
