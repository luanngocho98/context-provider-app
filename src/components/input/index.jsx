import React, { useState } from 'react'
import { Label, Input } from 'reactstrap'
import './index.css'


export default function TextField(props) {
  const { name, placeholder, value, onChange, className, errMessage, type = 'text', title } = props;

  const [isShowPassword, setIsShowPassword] = useState(false);

  return (
    <>
      <Label>
        {title}
      </Label>
      <Input 
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={onChange}
        className={`${className} ${type === 'password' && 'inputPassword'}`}
        type={!isShowPassword ? type : 'text'}
      />
      {
        type === 'password' && (
          <img
            alt='Fail' 
            src={
              !isShowPassword ? 
                'https://static.thenounproject.com/png/777494-200.png' : 
                  'https://icon-library.com/images/show-password-icon/show-password-icon-18.jpg'
            }
            className='icon'
            onClick={() => setIsShowPassword(!isShowPassword)}
          />
        )
      }
      <div className='errMessage'>
        {errMessage ? errMessage : <>&#160;</>}
      </div>
    </>
  )
}
