import React from 'react'
import { Route, Routes } from 'react-router-dom'
import About from '../pages/about'
import Home from '../pages/home'
import Login from '../pages/login'
import Register from '../pages/register'

export default function Router({ isAuthentication, setIsAuthentication, setStatusAlert }) {

  return (
    <Routes>
      <Route
        path='/'
        element={
          <Home
            isAuthentication={isAuthentication}
          />
        } 
      />
      <Route path='/about' element={<About/>} />
      <Route 
        path='/login'
        element={
          <Login
            setIsAuthentication={setIsAuthentication}
          />
        } 
      />
      <Route
        path='/register'
        element={
          <Register
            setIsAuthentication={setIsAuthentication}
            setStatusAlert={setStatusAlert}
          />
        } 
      />
    </Routes>
  )
}
