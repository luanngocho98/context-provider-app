
export const KEY = {
    USER: 'USER',
    USER_LIST: 'USER_LIST',
};

export const SetItemOnLocalStorage = (key, data) => {
    localStorage.setItem(
        key, typeof data === 'object' ? JSON.stringify(data) : data
    );
}

export const GetItemOnLocalStorage = (key) => {
    if(localStorage.getItem(key)) {
        if(
            localStorage.getItem(key).includes('[') && localStorage.getItem(key).includes(']') || 
            localStorage.getItem(key).includes('{') && localStorage.getItem(key).includes('}')
        ) {
            return JSON.parse(
                localStorage.getItem(
                    key
                )
            )
        } else {
            return localStorage.getItem(
                key
            );
        }
    } else {
        return null;
    }
};

export const ClearItemOnLocalStorage = (key) => {
    localStorage.removeItem(key);
};


export const EMAIL_REGEX =
  /^\s*(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$/;
