import React from 'react'
import Login from '../login'

// form bao gồm 3 ô input : Email, Password, Confirm password
// Create 1 user và lưu lên localStorage với [{user}, {user}] user: id, email, password
// Khi login là phải check user có khớp trên localStorage hay không ? thì mới đc login

export default function Register({ setIsAuthentication, setStatusAlert }) {
  return (<Login isRegister setIsAuthentication={setIsAuthentication} setStatusAlert={setStatusAlert} />)
}
