import React, { Fragment, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import LoadingButton from '../../components/button';
import TextField from '../../components/input'
import { EMAIL_REGEX, GetItemOnLocalStorage, KEY, SetItemOnLocalStorage } from '../../helper';
import './index.css'
import { v4 as uuidv4 } from 'uuid';

// admin@gmail.com --- admin

export default function Login({ setIsAuthentication, isRegister = false, setStatusAlert }) {
  const navigate = useNavigate();

  const [dataLogin, setDataLogin] = useState({
    email: '',
    password: '',
  });

  const [errors, setErrors] = useState({
    email: '',
    password: '',
  });

  const [dataRegister, setDataRegister] = useState({
    email: '',
    password: '',
    confirmPassword: '',
  });

  const [errorsRegister, setErrorsRegister] = useState({
    email: '',
    password: '',
    confirmPassword: '',
  });

  const handleChangeInput = (event) => {
    if(isRegister) {
      console.log(1)
      setDataRegister({
        ...dataRegister,
        [event.target.name]: event.target.value,
      });
      setErrorsRegister({
        ...errorsRegister,
        [event.target.name]: '',
      });
    } else {
      console.log(2)
      setDataLogin({
        ...dataLogin,
        [event.target.name]: event.target.value,
      });
      setErrors({
        ...errors,
        [event.target.name]: '',
      });
    }
  };

  const handleLogin = () => {
    const copyErrors = isRegister ? {...errorsRegister} : {...errors};
    let isPassValidate = true;

    if(isRegister) {
      for(const property in dataRegister) {
        if(!dataRegister[property]) {
          isPassValidate = false;
          copyErrors[property] = `Please enter ${property}`
        }
      };
      if(dataRegister.password !== dataRegister.confirmPassword) {
        isPassValidate = false;
        copyErrors['confirmPassword'] = 'Match password is error';
      };
      if(dataRegister.email && !EMAIL_REGEX.test(dataRegister.email)) {
        isPassValidate = false;
        copyErrors['email'] = 'Match email is error';
      }
    } else {
      for(const property in dataLogin) {
        if(!dataLogin[property]) {
          isPassValidate = false;
          copyErrors[property] = `Please enter ${property}`
        }
      };
    }

    if(!isPassValidate) {
      isRegister ? setErrorsRegister(copyErrors) : setErrors(copyErrors);
      return;
    }

    isRegister ? actionRegister() : actionLogin();

  };

  const actionLogin = () => {
    let user = null;
    const userOnLocalStorage = GetItemOnLocalStorage(KEY.USER_LIST) || [];
    if(!userOnLocalStorage?.length) {
      setErrors({
        ...errors,
        email: 'Invalid user',
      });
      return;
    }
    let isPassValidate = false;
    for(let i = 0; i < userOnLocalStorage.length; i++) {
      if(userOnLocalStorage[i].email === dataLogin.email && userOnLocalStorage[i].password === dataLogin.password) {
        isPassValidate = true;
        const userClone = {...userOnLocalStorage[i]}
        delete userClone.password
        user = userClone;
        break;
      } else {
        setErrors({
          email: userOnLocalStorage[i].email === dataLogin.email ? '' : 'Error email',
          password: userOnLocalStorage[i].password === dataLogin.password ? '' : 'Error password',
        });
      }
    }
    if(isPassValidate) {
      setIsAuthentication(true);
      navigate('/');
      SetItemOnLocalStorage(KEY.USER, user);
    } else {
      return;
    }
  };

  const actionRegister = () => {
    const userOnLocalStorage = GetItemOnLocalStorage(KEY.USER_LIST) || [];
    if(userOnLocalStorage?.length) {
      // suggest => find() method
      let isPassValidate = true;
      for(let i = 0; i < userOnLocalStorage.length; i++) {
        if(userOnLocalStorage[i].email === dataRegister.email) {
          isPassValidate = false;
          setErrorsRegister({
            ...errorsRegister,
            email: 'Invalid user email',
          });
          break;
        }
      }

      if(!isPassValidate) {
        setStatusAlert({
          isOpen: true,
          message: 'Error'
        });
        return;
      }

      userOnLocalStorage.push({
        id: uuidv4(),
        email: dataRegister.email,
        password: dataRegister.password,
      });
      SetItemOnLocalStorage(KEY.USER_LIST, userOnLocalStorage);
      setStatusAlert({
        isOpen: true,
        message: 'Success'
      });
    } else {
      userOnLocalStorage.push({
        id: uuidv4(),
        email: dataRegister.email,
        password: dataRegister.password,
      });
      SetItemOnLocalStorage(KEY.USER_LIST, userOnLocalStorage);
      setStatusAlert({
        isOpen: true,
        message: 'Success'
      });
    }
  };

  return (
    <Fragment>
      <div className='login-page'>
        <div className='form-login p-4'>
          <div className='mb-2'>
            <TextField
              name='email'
              value={isRegister ? dataRegister.email : dataLogin.email}
              placeholder='Please enter email'
              onChange={handleChangeInput}
              errMessage={isRegister ? errorsRegister.email : errors.email}
              title={'Email'}
            />
          </div>
          <div className='mb-2'>
            <TextField
              name='password'
              value={isRegister ? dataRegister.password : dataLogin.password}
              placeholder='Please enter password'
              onChange={handleChangeInput}
              errMessage={isRegister ? errorsRegister.password : errors.password}
              type='password'
              title={'Password'}
            />
          </div>
          {
            isRegister && (
              <div className='mb-4'>
                <TextField
                  name='confirmPassword'
                  value={dataRegister.confirmPassword}
                  placeholder='Please enter confirn register'
                  onChange={handleChangeInput}
                  errMessage={errorsRegister.confirmPassword}
                  type='password'
                  title={'Confirm register'}
                />
              </div>
            )
          }
          <div className='actions'>
            <LoadingButton
              color='danger'
              className='w-100'
              value={isRegister ? 'Register' : 'Login'}
              onClick={handleLogin}
            />
          </div>
        </div>
      </div>
    </Fragment>
  )
}
