import React, { Fragment, useEffect, useState } from 'react'
import { Avatar, Button, FormControl, IconButton, InputAdornment, InputLabel, OutlinedInput } from '@mui/material';
import { PhotoCamera, Visibility, VisibilityOff } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { brown } from '@mui/material/colors';

export default function Home({ isAuthentication }) {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [values, setValues] = useState({
    password: '',
    showPassword: false,
  });

  //https://www.geeksforgeeks.org/how-to-zoom-an-image-on-mouse-hover-using-css/

  // set height: calc(100vh - ...px);
  // overflow-y: scroll;
  // Add loading waiting call api

  useEffect(() => {
    if(isAuthentication) {
      fetch('https://631aa4f7dc236c0b1ee16924.mockapi.io/api/v1/films')
        .then(response => response.json())
        .then(posts => {
          setData(posts);
          setIsLoading(false);
        });
    } else {
      setData([]);
    }
  }, [isAuthentication]);

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleChange = (nameField) => (event) => {
    setValues({ ...values, [nameField]: event.target.value });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleCallApi = () => {
    setIsLoading(true);
    setTimeout(() => {
      //
      setIsLoading(false);
    }, 1500);
  }

  return (
    // <div>
    //   {data?.length ? JSON.stringify(data) : ''}
    // </div>
    <Fragment>
      <Button
        variant="outlined"
        sx={{
          color: 'red',
          border: '1px solid red',
          '&:hover': {
            border: '1.5px solid green',
          }
        }}
      >
        Hello World
      </Button> 

      <IconButton color="primary" aria-label="upload picture" component="label">
        <input hidden accept="image/*" type="file" />
        <PhotoCamera />
      </IconButton>

      <LoadingButton loading={isLoading} onClick={handleCallApi} variant="outlined">
        Submit
      </LoadingButton>

      <FormControl sx={{ m: 1, width: '25ch' }} variant="outlined">
          <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {values.showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label="Password"
          />
        </FormControl>

        <Avatar
          sx={{ bgcolor: brown[500], width: 55, height: 55 }}
          alt="Remy Sharp"
          src="https://www.simplilearn.com/ice9/free_resources_article_thumb/what_is_image_Processing.jpg"
        >
          Luan
        </Avatar>
    </Fragment>
  )
}
