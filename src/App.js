import React, { useState } from 'react'
import { BrowserRouter } from 'react-router-dom'
import Header from './components/header'
import { GetItemOnLocalStorage, KEY } from './helper';
import Router from './router'
import '@fontsource/roboto/300.css';

export default function App() {
  const [isAuthentication, setIsAuthentication] = useState(
    !!GetItemOnLocalStorage(KEY.USER) || false
  );
  const [statusAlert, setStatusAlert] = useState({
    isOpen: false,
    message: '',
  });

  return (
    <BrowserRouter>
      <Header
        isAuthentication={isAuthentication}
        setIsAuthentication={setIsAuthentication}
      />
      <div className='mt-4'>
        <Router
          isAuthentication={isAuthentication}
          setIsAuthentication={setIsAuthentication}
          setStatusAlert={setStatusAlert}
        />
      </div>
      {
        statusAlert.isOpen && (
          <h1>
            {statusAlert.message}
          </h1>
        )
      }
    </BrowserRouter>
  )
}
